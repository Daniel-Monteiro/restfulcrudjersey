package model.bean;

public class Banda {
	
	private int Id;
	private String nome;
	private String genero;
	private int ano_fund;
	
	public Banda() {
		
	}
	
	public Banda( String nome, String genero, int ano_fund) {
		super();
		this.nome = nome;
		this.genero = genero;
		this.ano_fund = ano_fund;
	}
	
	
	public Banda(int id, String nome, String genero, int ano_fund) {
		super();
		this.Id = id;
		this.nome = nome;
		this.genero = genero;
		this.ano_fund = ano_fund;
	}


	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public int getAno_fund() {
		return ano_fund;
	}
	public void setAno_fund(int ano_fund) {
		this.ano_fund = ano_fund;
	}
	
	
}

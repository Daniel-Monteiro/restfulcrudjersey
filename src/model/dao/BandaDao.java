package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.bean.Banda;

public class BandaDao {
	
	
	public Connection con;
	public ResultSet rs;
	public PreparedStatement ps;
	public String sql;

	public BandaDao() {
		this.con = ConectaBanco.getConnection();
	}

	public void setBanda(Banda banda) {

		sql = "INSERT into Banda(nome,genero,ano_fund) values (?,?,?)";

		try {

			ps = con.prepareStatement(sql);

			ps.setString(1, banda.getNome());
			ps.setString(2, banda.getGenero());
			ps.setInt(3, banda.getAno_fund());
			ps.executeQuery();

		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public Banda buscarBandaporId(int id) {
		
		
		sql = "SELECT * FROM Banda WHERE id = "+id;
		
		Banda banda = new Banda();
		
				
		try {
			
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			
			while (rs.next()) {
			     banda = new Banda(rs.getInt("id"),rs.getString("nome"), rs.getString("genero"), rs.getInt("ano_fund"));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return banda;
	}

	public List<Banda> getListaBandas() {

		sql = "SELECT * FROM Banda";

		List<Banda> lista = new ArrayList();

		try {

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {
				Banda banda = new Banda(rs.getInt("id"),rs.getString("nome"), rs.getString("genero"), rs.getInt("ano_fund"));
				lista.add(banda);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return lista;
	}

	public void deleteBanda(int Id) {

		sql = "DELETE from Banda where id =" + Id;

		try {

			ps = con.prepareStatement(sql);
			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	public void atualizaBanda(Banda banda) {
		
		sql = "UPDATE Banda SET nome = ?, genero = ?, ano_fund = ? WHERE id = ?";
		
	
		try {
			
			ps = con.prepareStatement(sql);
			ps.setString(1, banda.getNome());
			ps.setString(2, banda.getGenero());
			ps.setInt(3, banda.getAno_fund());
			ps.setInt(4, banda.getId());
			ps.execute();
			
		} catch (Exception e) {
			e.getMessage();
		}
	}
	
}

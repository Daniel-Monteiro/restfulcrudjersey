package model.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConectaBanco {
	
	private static final String URL = "jdbc:postgresql://localhost:5432/RestBanda";
	private static final String USER = "postgres";
	private static final String PASSWORD = "system";
	
	public static Connection getConnection() {
		
		Connection con = null;
		
		try {
			Class.forName("org.postgresql.Driver");
			con =  DriverManager.getConnection(URL, USER, PASSWORD);
			System.out.println("Conectado...");
		} catch (SQLException e) {
			System.out.println("Ocorreu Algo de Errado com a conex�o...");
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return con;
	}
	
	public static void main(String[]args) {
		ConectaBanco cb =  new ConectaBanco();
		cb.getConnection();
	}
}

package service;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;

import model.bean.Banda;
import model.dao.BandaDao;

@Path("/Jersey")
public class RestJR {
	
	private BandaDao bd = new BandaDao();
	private Gson gs =  new Gson();
	
	@GET
	@Path("/")
	@Produces(MediaType.TEXT_PLAIN)
	public String Hello(){
		return "Ol�, Sou o Rest, a seu dispor.";
	}
	
	@GET
	@Path("/getListaBanda")
	@Produces(MediaType.APPLICATION_JSON)
	public String getListaBanda() {
		List<Banda> lista = bd.getListaBandas();
		return gs.toJson(lista);
	}
	
	@POST
	@Path("/setBanda/{nome}/{genero}/{ano_fund}")
	@Produces("application/json")
	public void setBanda(@PathParam ("nome") String nome, @PathParam("genero") String genero, 
			@PathParam("ano_fund") int ano_fund) 
	{
		Banda banda =  new Banda(nome, genero, ano_fund);
	    bd.setBanda(banda);
	}
	
	@DELETE
	@Path("/deletabanda/{id}")
	@Produces("application/json")
	public void deletaBanda(@PathParam ("id") int Id) {
		bd.deleteBanda(Id);
	}
	
	@PUT
	@Path("/atualizabandaporId/{id}/{nome}/{genero}/{ano_fund}")
	@Produces("text/plain")
	public String atualizaBanda(@PathParam("id") int id, @PathParam("nome") String nome,
			@PathParam("genero") String genero, @PathParam("ano_fund") int ano_fund)
	{
		Banda banda = new Banda(id,nome,genero,ano_fund);
		bd.atualizaBanda(banda);
		return "Atualizado";
	}
	
	@GET
	@Path("/buscaBandaporId/{id}")
	@Produces("application/json")
	public String buscaBandaporId(@PathParam("id") int id) {
		Banda banda = bd.buscarBandaporId(id);
		return gs.toJson(banda);
	}
}
